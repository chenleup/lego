/*
@Time : 2020/12/29 11:31 下午
@Author : chenle
@File : common
@Software: GoLand
*/
package lego

import (
	"net/http"
)

type Rsp struct {
	Code int         `json:"code" comment:"状态码 0成功"`
	Msg  string      `json:"msg" comment:"错误信息"`
	Data interface{} `json:"data,omitempty" comment:"返回的数据"`
}

func (r *Rsp) GetMessage() string {
	return GetMsg(r.Code)
}

// 回复参数错误消息
func (r *Rsp) ReplyInvalidParam(c *Context, err error) {
	r.Code = INVALID_PARAMS
	r.Msg = err.Error()
	c.JSON(http.StatusOK, r)
}

// 自定义回复
func (r *Rsp) ReplyCode(c *Context, code int, msg string) {
	r.Code = code
	r.Msg = msg
	c.JSON(http.StatusOK, r)
}

// 回复成功， data为返回值
func (r *Rsp) ReplySuccess(c *Context, data interface{}) {
	r.Code = SUCCESS
	r.Msg = GetMsg(r.Code)
	r.Data = data
	c.JSON(http.StatusOK, r)
}

// 回复操作失败给前端，msg为失败原因
func (r *Rsp) ReplyFailOperation(c *Context, msg string) {
	r.Code = FAILE_TO_CREATE_OP
	r.Msg = msg
	c.JSON(http.StatusOK, r)
}

// 打印接口请求参数，path为接口路径，p为参数
func LogParams(c *Context, p interface{}) {
	Infof("request parameters of %s : %v", c.fullPath, p)
}

// 回复token验证失败消息
func (r *Rsp) ReplyTokenError(c *Context) {
	r.Code = ERROR_AUTH_CHECK_TOKEN_FAIL
	r.Msg = GetMsg(r.Code)
	c.JSON(http.StatusOK, r)
}

// 回复token过期消息
func (r *Rsp) ReplyTokenTimeout(c *Context) {
	r.Code = ERROR_AUTH_CHECK_TOKEN_TIMEOUT
	r.Msg = GetMsg(r.Code)
	c.JSON(http.StatusOK, r)
}

type ListRsp struct {
	List  interface{} `json:"list" comment:"列表数据"`
	Count int         `json:"count" comment:"总条数"`
}
