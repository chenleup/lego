module gitee.com/chenleup/lego

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang/protobuf v1.4.3
	github.com/gomodule/redigo v1.8.3
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.10
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.6.1
	github.com/ugorji/go/codec v1.2.2
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.11
)
