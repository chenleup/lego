## LEGO

lego 致力于打造开发人员开箱即用的后端框架,项目集成了 MySQL,Redis 等常用开发工具,自动完成日志配置,自动生成 showdoc 接口文档等,开发人员只需要专注于实现业务逻辑.

基于事件驱动的业务模型

![image-20201230142625671](README.assets/image-20201230142625671.png)



## 安装

```shell
 go get -u gitee.com/chenleup/lego@latest
```

## 示例

项目根目录创建目录 conf,创建系统配置文件 sys.yaml

```yaml
appname: legodemo
loglever: debug
runmode: debug
logpath: logs/
timeformat: 20060102
httpport: 8888
grpcport:
readtimeout: 60
writetimeout: 60

prod:

debug:
    mysql:
        address: 127.0.0.1:3306
        dbname: db_test
        username: root
        password: 123456
        maxconns: 64
        maxidleconns: 16
        connmaxlifetime: 3600000
    redis:
        host: 127.0.0.1:6379
        password: 123456
        db: 1
        maxIdle: 30
        maxActive: 30
        idleTimeout: 200
   
```

如果需要其他的应用配置,在 conf 目录创建 app.yaml 自行维护.

```go
package legodemo

import (
	"gitee.com/chenleup/lego"
	"log"
	"net/http"
)

// 接口请求 struct
type Req struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

// 数据库表
type User struct {
	lego.Model
	Name string
	Age  int
}

func main() {
    // 初始化应用
	app := lego.New().
    EnableMysql(&User{}).       // 开启 MySQL,并初始化,注册表数据
    EnableRedis().        // 开启 Redis,并初始化
	EnableCors(lego.DefaultCorsConfig()).     // 开启跨域
	AddBeforeFilters(func(c *lego.Context) {   // 注册前置拦截事件列表
		log.Println("before 11111")
	},
		func(c *lego.Context) {
			log.Println("before 222222")
		},
	).
	AddAfterFilters(func(c *lego.Context) {    // 注册后置拦截事件列表
		log.Println("after 11111")
	},
		func(c *lego.Context) {
			log.Println("after 222222")
		},
	)

    // 注册各个接口的处理事件列表
	app.GET("/hello",
		func(c *lego.Context) {
			name := c.Query("name")      // 获取 URL 查询参数
			log.Println("name: ", name)
		},
		func(c *lego.Context) {
			rsp := lego.Rsp{}
			rsp.ReplySuccess(c, map[string]interface{}{    // 返回
				"name":  "muse",
				"age":   3,
				"greet": "hello, my baby!",
			})
		},
        func(c *Context) {              // 事件在上一步打断,此函数不会执行,后置事件依然会执行
			log.Println("maybe never print")
		})
	app.POST("/world", func(c *lego.Context) {
		req := &Req{}
		rsp := lego.Rsp{}
		err := c.BindJSON(req)     // 绑定参数时,会自动将 req 放入 context 中,可供后面事件使用
		if err != nil {        // 参数会根据 struct 上 tag 定义的校验规则,自动校验,校验不通过返回具体错误的中文表述,可以直接返回前端,定义规则查看 validate.md
			rsp.ReplyInvalidParam(c, err)   // 出错则中断事件列表
		}
	},
		func(c *lego.Context) {
			rsp := lego.Rsp{}
			req, _ := c.Get("req") // 获取参数
			r, _ := req.(*Req)
			db := c.GetDB()        // 获取数据库连接
			user := &User{
				Name: r.Name,
				Age:  r.Age,
			}
			err := db.Save(user).Error
			if err != nil {
				lego.Error(err.Error())   // 日志生成在 logs 目录,可在配置文件修改
				rsp.ReplyFailOperation(c, "数据库异常")
			}
			err = c.Cache.Set("userinfo", user, 0)  // 使用redis 缓存数据
			if err != nil {
				lego.Error(err.Error())
				rsp.ReplyFailOperation(c, "缓存异常")
			}
			rsp.ReplySuccess(c, map[string]interface{}{"id": user.ID})
		},
	)
	app.POST("/foo", func(c *lego.Context) {
		lego.Info("foo and bar")
		c.JSON(http.StatusOK, map[string]string{"foo": "bar"})
	})

	app.GET("/connect", func(c *lego.Context) {
		rsp := lego.Rsp{}
		ws, err := c.OpenWebsocket()     // 开启 websocket
		if err != nil {
			rsp.ReplyFailOperation(c, "开启长连接失败")
		}
		mtype, content, err := ws.ReadMessage()
		log.Println(mtype, string(content))
	})
    app.Run()      // 开启服务
}


```



### showdoc 文档生成

配置文件sys.yaml中加入如下参数

```yaml
openshowdoc: true

debug:
    showdoc:
        apikey: 
        apitoken: 
        showdocurl: 

```

用 postman 测试接口时,传入 header, 中文需要转码

title   :   接口文档标题

dir  :    接口文档所属文件夹