/*
@Time : 2020/12/29 12:08 下午
@Author : chenle
@File : engine
@Software: GoLand
*/
package lego

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"
	gorm2 "gorm.io/gorm"
)

const defaultMultipartMemory = 32 << 20

type Engine struct {
	Config             *Conf // 配置
	SysLogger          zerolog.Logger
	AppLogger          zerolog.Logger
	DB                 *gorm.DB
	DB2                *gorm2.DB
	Cache              *Cache
	MaxMultipartMemory int64
	BeforeFilters      Events
	AfterFilters       Events
	EnabledCors        bool
	CorsEvent          Event
	OpenShowDoc        bool
	OpenCors           bool
	useGorm2           bool
	corsHeaders        []string
}
