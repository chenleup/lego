/*
@Time : 2020/12/29 10:33 上午
@Author : chenle
@File : lego_test
@Software: GoLand
*/
package lego

import (
	"log"
	"net/http"
	"testing"
)

type Req struct {
	Name string `json:"name" comment:"姓名" validate:"required"`
	Age  int    `json:"age" comment:"年龄" validate:"required"`
}

type UserTest struct {
	Model2
	Name string
	Age  int
}

func TestNew(t *testing.T) {
	app := New().EnableMysql().EnableRedis()
	app.MigrateModels(&UserTest{})
	app.EnableCors(DefaultCorsConfig())
	app.AddBeforeFilters(func(c *Context) {
		log.Println("before 11111")
	},
		func(c *Context) {
			log.Println("before 222222")
		},
	)
	app.AddAfterFilters(func(c *Context) {
		log.Println("after 11111")
	},
		func(c *Context) {
			log.Println("after 222222")
		},
	)

	app.GET("/hello",
		func(c *Context) {
			name := c.Query("name")
			log.Println("name: ", name)
		},
		func(c *Context) {
			rsp := Rsp{}
			rsp.ReplySuccess(c, map[string]interface{}{
				"name":  "muse",
				"age":   3,
				"greet": "hello, my baby!",
			})
			//fmt.Fprintf(c.Writer, "hello,muse333333!")
		},
		func(c *Context) {
			log.Println("maybe never print")
		})

	app.POST("/world", func(c *Context) {
		req := &Req{}
		rsp := Rsp{}
		err := c.BindJSON(req)
		if err != nil {
			rsp.ReplyInvalidParam(c, err)
		}
	},
		func(c *Context) {
			rsp := Rsp{}
			req, _ := c.Get("req")
			r, _ := req.(*Req)
			db := c.GetDB2()
			user := &UserTest{
				Name: r.Name,
				Age:  r.Age,
			}

			err := db.Save(user).Error
			if err != nil {
				SysInfo(err.Error())
				rsp.ReplyFailOperation(c, "数据库异常")
			}
			err = c.Cache.Set("userinfo", user, 0)
			if err != nil {
				SysInfo(err.Error())
				rsp.ReplyFailOperation(c, "缓存异常")
			}
			rsp.ReplySuccess(c, map[string]interface{}{"id": user.ID})
		},
	)
	app.POST("/foo", func(c *Context) {
		//Info("foo and bar")
		printe(c)
		c.JSON(http.StatusOK, map[string]string{"foo": "bar"})
	})

	app.GET("/connect", func(c *Context) {
		rsp := Rsp{}
		ws, err := c.OpenWebsocket()
		if err != nil {
			rsp.ReplyFailOperation(c, "开启长连接失败")
		}
		mtype, content, err := ws.ReadMessage()
		log.Println(mtype, string(content))
	})

	g := app.Group("/haha", func(c *Context) {
		log.Println("/haha")
	})
	g.POST("/hehe", func(c *Context) {
		log.Println("/haha/hehe")
	})
	g.GET("/momo",
		func(c *Context) {
			log.Println("/haha/momo---1")
		},
		func(c *Context) {
			log.Println("/haha/momo---2")
			c.AbortWithStatus(200)
		})
	Debug("sdgfsdfsdf")
	app.Run()
}

func printe(c *Context) {
	c.Logger.Print("sdfsdfdsfdsfdsfdsa")
}
