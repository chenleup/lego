/*
@Time : 2020/12/30 11:44 上午
@Author : chenle
@File : websocket
@Software: GoLand
*/
package lego

import (
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
	HandshakeTimeout: 2 * time.Second,
	ReadBufferSize:   1024,
	WriteBufferSize:  1024,
}
